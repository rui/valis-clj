(ns dev.ruivieira.valis.k8s.k8s-test
  (:require [clojure.test :refer :all])
  (:require [dev.ruivieira.valis.k8s.k8s :refer [resource-apply-str]]))


(deftest test-apply-str
  (testing "Apply string generation"
    (let [resource-map {:kind "Pod" :metadata {:name "my-pod"} :spec { :container "my-container"}}
          resource-yaml "kind: Pod\nmetadata:\n  name: my-pod\nspec:\n  container: my-container\n"
          base-command "kubectl apply -f -"
          ns-command "kubectl apply -n my-namespace -f -"]
      ;; Test without namespace
      (let [{:keys [command input]} (resource-apply-str resource-map)]
        (is (= command base-command))
        (is (= input resource-yaml)))

      ;; Test with namespace
      (let [{:keys [command input]} (resource-apply-str resource-map "my-namespace")]
        (is (= command ns-command))
        (is (= input resource-yaml))))))
