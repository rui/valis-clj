(ns dev.ruivieira.valis.core-test
  (:require [clojure.string :as string]
            [clojure.test :refer :all]
            [dev.ruivieira.valis.core :refer :all]))

(deftest test-date-difference-in-days
  (testing "difference between the same date is zero"
    (is (= 0 (date-difference-in-days "2023-07-05" "2023-07-05"))))
  (testing "difference between consecutive dates is one"
    (is (= 1 (date-difference-in-days "2023-07-05" "2023-07-06"))))
  (testing "difference is negative"
    (is (= -1 (date-difference-in-days "2023-07-06" "2023-07-05")))))

(deftest test-expand-file-name
  (testing "Expand file names"
    (let [user-home (System/getProperty "user.home")]
      ;; Test with absolute path
      (is (= (expand-file-name "/tmp/test.txt") "/tmp/test.txt"))

      ;; Test with relative path
      (is (= (expand-file-name "test.txt") (string/join "/" [(System/getProperty "user.dir") "test.txt"])))

      ;; Test with tilde
      (is (= (expand-file-name "~/test.txt") (string/join "/" [user-home "test.txt"])))

      ;; Test with tilde in the middle of the path (shouldn't be expanded)
      (is (= (expand-file-name "folder/~/test.txt") (string/join "/" [(System/getProperty "user.dir") "folder/~/test.txt"]))))))
